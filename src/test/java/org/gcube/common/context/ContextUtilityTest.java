package org.gcube.common.context;

import org.gcube.common.ContextTest;
import org.gcube.common.authorization.library.provider.AccessTokenProvider;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.authorization.utils.manager.SecretManager;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ContextUtilityTest extends ContextTest {

	private static final Logger logger = LoggerFactory.getLogger(ContextUtilityTest.class);
	
	@Test
	public void testGetContext() throws Exception {
		SecretManager secretManager = SecretManagerProvider.instance.get();
		String context = secretManager.getContext();
		String newToken = secretManager.getCurrentSecretHolder().getSecrets().first().getToken();
		ContextTest.afterClass();
		
		ScopeProvider.instance.set(context);
		String gotContext = ContextUtility.getCurrentContextFullName();
		logger.debug("Expected context is {} - Got Context is {}", context, gotContext);
		Assert.assertTrue(context.compareTo(gotContext)==0);
		ScopeProvider.instance.reset();
		
		
		String oldToken = ContextTest.properties.getProperty("old_token_gcube");
		SecurityTokenProvider.instance.set(oldToken);
		gotContext = ContextUtility.getCurrentContextFullName();
		logger.debug("Expected context is {} - Got Context is {}", context, gotContext);
		Assert.assertTrue(context.compareTo(gotContext)==0);
		SecurityTokenProvider.instance.reset();
		
		
		AccessTokenProvider.instance.set(newToken);
		gotContext = ContextUtility.getCurrentContextFullName();
		logger.debug("Expected context is {} - Got Context is {}", context, gotContext);
		Assert.assertTrue(context.compareTo(gotContext)==0);
		AccessTokenProvider.instance.reset();
	}
	
}
