package org.gcube.common.http;

import org.gcube.common.gxhttp.request.GXHTTPStringRequest;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class GXHTTPUtility {

	public static GXHTTPStringRequest getGXHTTPStringRequest(String address) {
		GXHTTPStringRequest gxHTTPStringRequest = GXHTTPStringRequest.newRequest(address);
		return gxHTTPStringRequest;
	}
	
}
