package org.gcube.common.context;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.common.authorization.client.Constants;
import org.gcube.common.authorization.library.AuthorizationEntry;
import org.gcube.common.authorization.library.provider.AccessTokenProvider;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.keycloak.model.AccessToken;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.common.scope.impl.ScopeBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@SuppressWarnings("deprecation")
public class ContextUtility {

	private static final Logger logger = LoggerFactory.getLogger(ContextUtility.class);
	
	public static String getCurrentContextFullName() {
		String context = ScopeProvider.instance.get();
		if(context==null) {
			logger.trace("ScopeProvider is null. Going to get context from SecurityTokenProvider.");
			String token = SecurityTokenProvider.instance.get();
			if(token!=null) {
				logger.trace("Found SecurityTokenProvider");
				try {
					AuthorizationEntry authorizationEntry = Constants.authorizationService().get(token);
					return authorizationEntry.getContext();
				}catch (Exception e) {
					throw new RuntimeException(e);
				}
			}else {
				logger.trace("ScopeProvider AND SecurityTokenProvider are null. Going to get context from AccessTokenProvider.");
				token = AccessTokenProvider.instance.get();
				if(token!=null) {
					logger.trace("Found AccessTokenProvider");
					String realUmaTokenEncoded = token.split("\\.")[1];
					String realUmaToken = new String(Base64.getDecoder().decode(realUmaTokenEncoded.getBytes()));
					ObjectMapper mapper = new ObjectMapper();
					try {
						AccessToken accessToken = mapper.readValue(realUmaToken, AccessToken.class);
						String[] audience = accessToken.getAudience();
						for (String aud : audience) {
							if (aud != null && aud.compareTo("") != 0) {
								try {
									String contextToBeValidated = URLDecoder.decode(aud, StandardCharsets.UTF_8.toString());
									ScopeBean scopeBean = new ScopeBean(contextToBeValidated);
									context = scopeBean.toString();
									return context;
								} catch (Exception e) {
									// logger.trace("Invalid context name for audience {} in access token. Trying next one if any.", aud, e);
								}
							}
						}
						throw new Exception("Unable to find valid context in audience: " + audience.toString());
					}catch(Exception e){
						throw new RuntimeException("Error parsing JWT token.", e);
					}
				}else {
					logger.trace("ScopeProvider, AccessTokenProvider AND SecurityTokenProvider are null. There is no other possibility to get the context.");
					throw new RuntimeException("ScopeProvider, AccessTokenProvider AND SecurityTokenProvider are null. There is no other possibility to get the context.");
				}
			}
		}
		logger.trace("Found ScopeProvider");
		return context;
	}
	
}
