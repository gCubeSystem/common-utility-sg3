This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Common Utility For Smartgears 3.X.X

## [v1.1.0-SNAPSHOT]

- Added evaluation of SecurityTokenProvider to calculate the context

## [v1.0.1]

- Fixed issue on getCurrentContextFullName

## [v1.0.0]

- First Version

